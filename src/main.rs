use lifx_core::{BuildOptions, Message, RawMessage, HSBK};
use std::{net::UdpSocket, time::Duration};

struct Light<'a> {
    mac: Option<u64>,
    ip: &'a str,
    port: &'a str,
}

impl<'a> Light<'a> {
    fn get_url(&self) -> String {
        format!("{}:{}", self.ip, self.port)
    }
}

fn get_lights<'a>() -> Vec<Light<'a>> {
    vec![
        Light {
            mac: Some(0x186D57D573D0),
            ip: "10.0.0.12",
            port: "56700",
        },
        Light {
            mac: Some(0x4FD656D573D0),
            ip: "10.0.0.14",
            port: "56700",
        },
    ]
}

fn get_percentage(percent: u16) -> u16 {
    (u16::MAX as f32 * (percent as f32 / 100.0f32)) as u16
}

fn main() {
    let sock = UdpSocket::bind("0.0.0.0:56700").unwrap();
    sock.set_broadcast(true).unwrap();

    let lights = get_lights();
    
    let mut i: i16 = 0;
    let mut dir = 1;

    loop {
        for light in &lights {
            let opts = BuildOptions {
                target: light.mac,
                ack_required: false,
                res_required: false,
                sequence: 0,
                source: 12345678,
            };

            let color = HSBK {
                hue: 0,
                saturation: 0,
                brightness: get_percentage(i as u16),
                kelvin: 6300,
            };
            
            println!("{}", i);
            
            i = i + dir;

            if i >= 100 || i <= 0 {
                dir = dir * -1;
            }
            
            std::thread::sleep(Duration::from_millis(100));

            let msg = Message::LightSetColor {
                reserved: 0,
                color,
                duration: 100,
            };

            let raw = RawMessage::build(&opts, msg).unwrap();
            let bytes = raw.pack().unwrap();
            sock.send_to(&bytes, &light.get_url()).unwrap();

            let msg = Message::LightSetPower {
                level: 65535,
                duration: 1,
            };

            let raw = RawMessage::build(&opts, msg).unwrap();
            let bytes = raw.pack().unwrap();
            sock.send_to(&bytes, &light.get_url()).unwrap();
        }
    }
}
